#include <iostream>
#include <SDLWindowInterface.h>

static int quit = 0;

SDLWindowInterface windowInterface;

void main_loop()
{
    if(windowInterface.OnUpdate())
    {
        quit = 1;
    }
    /* Clear context */
    glClearColor(1.0f,0.0f,0.0f,1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

int main()
{
    std::cout << "hello world!!" << std::endl;

    windowInterface.OnInit(30, 30, 640, 480);

#ifdef WEB
    emscripten_set_main_loop(main_loop, -1, 1);
#else
    while(quit == 0)
    {
        main_loop();
    }
#endif

    windowInterface.OnUninit();

    return 0;
}
