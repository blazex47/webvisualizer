from conans import ConanFile, CMake

class VisualizerConan(ConanFile):
   settings = "os", "compiler", "build_type", "arch"
   generators = "cmake", "gcc", "txt"
   default_options = {}

   def requirements(self):
      #self.requires("sdl2/2.0.9@bincrafters/stable")
      self.requires("eziengine/1.0.0@myremote/stable")

   def imports(self):
      self.copy("*.dll", dst="bin", src="bin") # From bin to bin
      self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

   def build(self):
      cmake = CMake(self)
      if self.settings.os == "Linux":
         cmake.definitions["PLATFORM"] = "UBUNTU"
      elif self.settings.os == "Emscripten":
         cmake.definitions["PLATFORM"] = "WEB"
      cmake.configure()
      cmake.build()