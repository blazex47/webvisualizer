#!/usr/bin/env python
from http.server import  HTTPServer, SimpleHTTPRequestHandler
 
port=6080
 
SimpleHTTPRequestHandler.extensions_map['.wasm'] ='application/wasm'
 
httpd = HTTPServer(('localhost', port),SimpleHTTPRequestHandler)
 
httpd.serve_forever()