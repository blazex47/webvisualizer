#include <SDLWindowInterface.h>

SDLWindowInterface::SDLWindowInterface()
{

}

SDLWindowInterface::~SDLWindowInterface()
{

}

void SDLWindowInterface::OnInit(int x, int y, int width, int height)
{
    SDL_Init(SDL_INIT_VIDEO);

    mWindow = SDL_CreateWindow("SDL2/OpenGL Demo",  x,  y,  width, height, SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
        
    /* Create an OpenGL context associated with the window. */
    mGlContext = SDL_GL_CreateContext(mWindow);

#ifdef WEB
    SDL_SetHint(SDL_HINT_EMSCRIPTEN_KEYBOARD_ELEMENT, "#canvas");
#endif

    /* This makes our buffer swap syncronized with the monitor's vertical refresh */
    SDL_GL_SetSwapInterval(1);
}

bool SDLWindowInterface::OnUpdate()
{
    bool quit = false;

    SDL_GL_SwapWindow(mWindow);

    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
            {
                quit = true;
                break;
            }       
        }
    }

    return quit;
}

void SDLWindowInterface::OnUninit()
{
    SDL_GL_DeleteContext(mGlContext);
    SDL_DestroyWindow(mWindow);
    SDL_Quit();
}   