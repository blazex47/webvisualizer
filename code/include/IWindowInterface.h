class IWindowInterface
{
    public:
    IWindowInterface(){}
    virtual ~IWindowInterface(){}

    virtual void OnInit(int x, int y, int width, int height) = 0;
    virtual bool OnUpdate() = 0;
    virtual void OnUninit() = 0;
};
