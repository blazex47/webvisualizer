#include "IWindowInterface.h"

#include <SDL.h>
#ifdef WEB
#include <SDL_opengles2.h>
#include <emscripten/emscripten.h>
#else
#include <SDL_opengl.h>
#include <SDL_opengl_glext.h>
#endif

class SDLWindowInterface: public IWindowInterface
{
    public:
    SDLWindowInterface();
    virtual ~SDLWindowInterface();

    virtual void OnInit(int x, int y, int width, int height);
    virtual bool OnUpdate();
    virtual void OnUninit(); 
    private:
    SDL_Window *mWindow = nullptr;
    SDL_GLContext mGlContext;
};
